grammar FOOL;

@lexer::members {
   //there is a much better way to do this, check the ANTLR guide
   //I will leave it like this for now just becasue it is quick
   //but it doesn't work well
   public int lexicalErrors=0;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/
  
prog   : exp SEMIC                 #singleExp
       | let exp SEMIC             #letInExp
       |  (classdec)+ (let)? exp SEMIC             #classExp
       ;

let       : LET (dec SEMIC)+ IN ;

vardec  : type ID ;

varasm     : vardec ASM exp ;

fun    : typefun ID LPAR ( vardec ( COMMA vardec)* )? RPAR (let)? exp ;

dec   : varasm           #varAssignment
      | fun              #funDeclaration
      | methodCall       #methodCallDec
      ;
         
   
type   : INT  
        | BOOL
        | ID
      ;

typefun   : INT
        | BOOL
        | VOID
        | ID
      ;

exp    :  ('-')? left=term ((PLUS | MINUS) right=exp)?
      ;
   
term   : left=factor ((TIMES | DIV) right=term)?
      ;

factor : (NOT)? left=value (( EQ | MAJOR | MINOR | MAJOREQ | MINOREQ | AND | OR) right=value)?;

methodCall: ID DOT ID ( LPAR (exp (COMMA exp)* )? RPAR )?;

value  :  INTEGER                           #intVal
      | (NOT)? ( TRUE |  FALSE)            #boolVal
      | LPAR exp RPAR                      #baseExp
      | IF cond=exp THEN CLPAR thenBranch=exp CRPAR ELSE CLPAR elseBranch=exp CRPAR  #ifExp
      | ID                                             #varExp
      | ID ( LPAR (exp (COMMA exp)* )? RPAR )?         #funExp
      | methodCall                                     #dotClassExp
      | NEW ID ( LPAR (exp (COMMA exp)* )? RPAR )?      #newClassExp;


classdec:   CLASS ID (EXTENDS ID)? LPAR (vardec(COMMA vardec)*)? RPAR CLPAR (varasm SEMIC)* (fun SEMIC)+ CRPAR SEMIC ;

/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/
SEMIC  : ';' ;
COLON  : ':' ;
COMMA  : ',' ;
EQ     : '==' ;
MAJOR  : '>' ;
MINOR  : '<' ;
MAJOREQ  : '>=' ;
MINOREQ  : '<=' ;
ASM    : '=' ;
PLUS   : '+' ;
MINUS  : '-' ;
TIMES  : '*' ;
DIV    : '/' ;
TRUE   : 'true' ;
FALSE  : 'false' ;
AND    : '&&' ;
OR     : '||' ;
NOT    : '!' ;
LPAR   : '(' ;
RPAR   : ')' ;
CLPAR  : '{' ;
CRPAR  : '}' ;
IF        : 'if' ;
THEN   : 'then' ;
ELSE   : 'else' ;
//PRINT : 'print' ; 
LET    : 'let' ;
IN     : 'in' ;
VAR    : 'var' ;
FUN    : 'fun' ;
INT    : 'int' ;
BOOL   : 'bool' ;
VOID   : 'void' ;
NEW    : 'new' ;
CLASS  :  'class';
EXTENDS: 'extends';
DOT     : '.';

//Numbers
fragment DIGIT : '0'..'9';    
INTEGER       :(MINUS)? DIGIT+;

//IDs
fragment CHAR  : 'a'..'z' |'A'..'Z' ;
ID              : CHAR (CHAR | DIGIT)* ;

//ESCAPED SEQUENCES
WS              : (' '|'\t'|'\n'|'\r')-> skip;
LINECOMENTS    : '//' (~('\n'|'\r'))* -> skip;
BLOCKCOMENTS    : '/*'( ~('/'|'*')|'/'~'*'|'*'~'/'|BLOCKCOMENTS)* '*/' -> skip;




 //VERY SIMPLISTIC ERROR CHECK FOR THE LEXING PROCESS, THE OUTPUT GOES DIRECTLY TO THE TERMINAL
 //THIS IS WRONG!!!!
ERR     : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 
