push 0
//c
push 1
push 2
push 3
push 3
push class0
new

//d
push 191
push 12
push 13
push 3
push class1
new

//w
push 20
push 20
push 20
push 3
push class2
new

lfp
push -4
lfp
add
lw
ssp
lw
push 7
add
lc
js
halt

field0:
push 12
lra
js

field1:
push 10
lra
js

method0:
cfp
lra
push field0
js
push 1
lfp
add
lw
add
push field1
js
mult
print
srv
sra
pop
pop
sfp
lrv
lra
js

method1:
cfp
lra
push 1
lfp
lw
add
lw
print
srv
sra
pop
sfp
lrv
lra
js

method2:
cfp
lra
push 200
print
srv
sra
pop
sfp
lrv
lra
js

method3:
cfp
lra
push 89
print
srv
sra
pop
sfp
lrv
lra
js

field2:
push 4
lra
js

method4:
cfp
lra
push 1
lfp
lw
add
lw
push field1
js
add
print
srv
sra
pop
sfp
lrv
lra
js

method5:
cfp
lra
push 1
lfp
lw
add
lw
print
srv
sra
pop
sfp
lrv
lra
js

method6:
cfp
lra
push 5
print
srv
sra
pop
sfp
lrv
lra
js

field3:
push 9
lra
js

method7:
cfp
lra
push 500
print
srv
sra
pop
sfp
lrv
lra
js

method8:
cfp
lra
push 1
lfp
add
lw
print
srv
sra
pop
pop
sfp
lrv
lra
js

class0:
push field0
push field1
push method0
push method1
push method2
push method3

class1:
push field1
push field2
push method0
push method3
push method4
push method5
push method6

class2:
push field1
push field3
push method0
push method3
push method5
push method6
push method7
push method8
