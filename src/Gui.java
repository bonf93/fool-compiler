


import lib.FOOLlib;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.filechooser.*;


public class Gui extends JPanel
        implements ActionListener {
    static private final String newline = "\n";
    JButton CompileButton, InterpreterButton, RunButton;
    JTextArea log;
    JFileChooser fc;

    public Gui() {
        super(new BorderLayout());

        log = new JTextArea(15,25);
        log.setMargin(new Insets(5,5,5,5));
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);

        //Create a file chooser
        fc = new JFileChooser();


        CompileButton = new JButton("Compile a file ...");
        CompileButton.addActionListener(this);

        InterpreterButton = new JButton("Interpreter a File...");
        InterpreterButton.addActionListener(this);


        RunButton = new JButton("Run a file ...");
        RunButton.addActionListener(this);



        Font myFont = new Font("Serif", Font.ITALIC, 12);
        CompileButton.setFont(myFont);
        InterpreterButton.setFont(myFont);
        RunButton.setFont(myFont);

        JPanel buttonPanel = new JPanel(); //use FlowLayout
        buttonPanel.add(CompileButton);
        buttonPanel.add(InterpreterButton);
        buttonPanel.add(RunButton);



        //Add the buttons and the log to this panel.
        add(buttonPanel, BorderLayout.PAGE_START);
        add(logScrollPane, BorderLayout.CENTER);
    }

    public void actionPerformed(ActionEvent e) {
        String ris="";
        FOOLlib.reset();
        log.setText(null);
        /** Compile Button **/
        if (e.getSource() == CompileButton) {
            int returnVal = fc.showOpenDialog(Gui.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                System.out.println(file.toString());
                if (file.toString().contains(".asm")) {
                    System.out.println("il file non è in formato .fool");
                    log.append("il file non è in formato.fool");
                }
                else{
                    try {
                        ris = Test2.compile(file.toString());
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                    log.append("Compiling :" + file.toString() + newline + newline);
                    List<String> fileArray = null;
                    try {
                        fileArray = Files.readAllLines(Paths.get(ris));
                        ris = fileArray.toString();
                    } catch (IOException e1) {
                        log.append(ris + newline + newline);
                        e1.printStackTrace();
                    }
                    for (String a : fileArray) {
                        log.append(a + newline);
                    }

                    if (ris.indexOf("ERROR") == -1) log.append("copiled succefully" + newline);
                    log.append(newline);
                }
            } else {
                log.append("Compile command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());

        }

        /** Interpreter Button **/
        else if (e.getSource() == InterpreterButton) {
            int returnVal = fc.showOpenDialog(Gui.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                if (!file.toString().contains(".asm")) {
                    System.out.println("il file non è in formato .asm");
                    log.append("il file non è in formato .asm");
                }
                else {

                    log.append("interpreting: " + file + newline + newline);

                    try {
                        ris = Test2.interpreter(file.toString());
                    } catch (Exception e1) {
                        System.out.println("catch: " + ris);
                        log.append(ris + newline + newline);
                        e1.printStackTrace();
                    }

                    log.append(ris + newline);
                    if (ris.indexOf("ERROR") == -1) log.append("Finish succefully" + newline + newline);
                }
            } else {
                log.append("Interpreted command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());
        }


        /** Run Button**/
        else if (e.getSource()== RunButton) {
            int returnVal = fc.showOpenDialog(Gui.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                if (file.toString().contains(".asm")) {
                    System.out.println("il file non è in formato .fool");
                    log.append("il file non è in formato.fool");
                } else {

                    try {
                        ris = Test2.main(file.toString());
                    } catch (Exception e1) {
                        log.append(ris + newline + newline);
                        e1.printStackTrace();
                    }

                    log.append("run :" + file.toString() + newline + newline);
                    if (ris != "0") log.append(ris + newline + newline);
                    if (ris.indexOf("ERROR") == -1) log.append("Run succefully" + newline + newline);

                }
            }
            else {
                log.append("Interpreted command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());


        }



    }

    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = Gui.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("FileChooserDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add content to the window.
        frame.add(new Gui());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }
}