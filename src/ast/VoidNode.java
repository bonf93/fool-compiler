package ast;

import util.Environment;
import util.SemanticError;

import java.util.ArrayList;

public class VoidNode implements Node {


  public VoidNode() {

  }
  
  public String toPrint(String s) {

      return s+ " Void \n";
  }
  
  public Node typeCheck() {
    return new VoidTypeNode();
  }    
  
  @Override
 	public ArrayList<SemanticError> checkSemantics(Environment env) {

 	  return new ArrayList<SemanticError>();
 	}
  
  public String codeGeneration() {
		return "";
	  }
         
}  