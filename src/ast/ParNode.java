package ast;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import util.Environment;
import util.SemanticError;

public class ParNode implements Node {

  public String id;
  public Node type;
  public int offset;
  
  public ParNode (String i, Node t, int o) {
   id=i;
   type=t;
   offset= o;
  }
  
  public String getId(){
	  return id;
  }

  public Node getType(){
	  return type;
  }
  
  @Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {
      LinkedHashMap<String,STentry> hm = env.symTable.get(env.scopeNumber);
      STentry entry = new STentry(env.nestingLevel,type, offset); //separo introducendo "entry"
        hm.put(id,entry);
	  return new ArrayList<SemanticError>();
	}
  
  public String toPrint(String s) {
	  return s+"Par:" + id +"\n"
			 +type.toPrint(s+"  ") ; 
  }
  
  //non utilizzato
  public Node typeCheck () {
     return type;
  }

  
  //non utilizzato
  public String codeGeneration() {
		return "";
  }
    
}  