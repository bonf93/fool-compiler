package ast;

import java.util.ArrayList;

import util.Environment;
import util.SemanticError;

public class PrintNode implements Node {

    private Node val;

    public PrintNode (Node v) {
        val=v;
    }

    public String toPrint(String s) {
        return s+"Print\n" + val.toPrint(s+"  ") ;
    }

    public Node typeCheck() {
        if(!(val.typeCheck() instanceof ClassTypeNode))
            return new VoidTypeNode();
        else {
            System.out.println("Wrong use of an instance of the class "+((ClassTypeNode) val.typeCheck()).type+"  for function print.");
            System.exit(0);
        }
        return null;
    }

    @Override
    public ArrayList<SemanticError> checkSemantics(Environment env) {

        return val.checkSemantics(env);
    }

    public String codeGeneration() {
        return val.codeGeneration()+"print\n";
    }

}  