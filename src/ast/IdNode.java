package ast;

import java.util.ArrayList;

import util.Environment;
import util.SemanticError;
import util.VirtualTable;
import util.VirtualTableEntry;

public class IdNode implements Node {

  private String id;
  private STentry entry;
  private int nestinglevel;
  private boolean field = false;
  private String classe;
  
  public IdNode (String i, String c) {
    id=i;
    classe = c;
  }

  public String getId(){
      return id;
  }

  public String toPrint(String s) {
	return s+"Id:" + id + " at nestlev " + nestinglevel +"\n" + (entry != null?entry.toPrint(s+"  "): "") ;
  }


  @Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {
	  
	  //create result list
	  ArrayList<SemanticError> res = new ArrayList<SemanticError>();
	  
	  int j=env.scopeNumber;
	  STentry tmp=null;
	  boolean classField = false;
          while (j >= 0 && tmp == null)
              tmp = (env.symTable.get(j--)).get(id);
          if (tmp == null){
                  res.add(new SemanticError("Id " + id + " not declared"));
          }

          else {
              for (String s : env.HMClassType.keySet()){
                String SuperClass = env.HMClass.get(s).getSuperClassId();
                  if(env.symTable.get(j+1).keySet().contains(s)){
                        if (env.HMClass.get(s).getFieldsName().contains(id) || (SuperClass!=null && env.HMClass.get(SuperClass).getFieldsName().contains(id)))
                            classField=true;
                  }
              }
              entry = tmp;
              nestinglevel = env.nestingLevel;
              if(classField){
                  if(env.HMClass.get(env.HMClass.keySet().toArray()[j]).getFieldsName().contains(id)){
                      field = true;
                  }
              }
          }



	  return res;
	}
  
  public Node typeCheck () {
	if ( entry.getType() instanceof ArrowTypeNode ) { //
	  System.out.println("Wrong usage of function identifier");
      System.exit(0);
    }	 
    return entry.getType();
  }
  
  public String codeGeneration() {

	  if(field){
          String s = new String();


          if (VirtualTable.VTable.size()>0 && classe != null) {
              VirtualTableEntry vt = VirtualTable.VTable.get(classe);
              s = vt.getFieldByName(id);
          }
          return "push " + s + "\n" + "js\n"; //metto l'indirizzo al campo sullo steck

      }
    
      else{
          String getAR="";
          for (int i=0; i<nestinglevel-entry.getNestinglevel(); i++)
              getAR+="lw\n";


          return "push "+entry.getOffset()+"\n"+ //metto offset sullo stack
                  "lfp\n"+getAR+ //risalgo la catena statica
                  "add\n"+
                  "lw\n"; //carico sullo stack il valore all'indirizzo ottenuto

      }
  }
}  