package ast;

import lib.FOOLlib;
import util.Environment;
import util.SemanticError;

import java.util.ArrayList;

public class NotNode implements Node {

    private Node left;

    public NotNode(Node l) {
        left=l;
    }

    public String toPrint(String s) {
        return s+"Not\n" + left.toPrint(s+"  ");
    }

    @Override
    public ArrayList<SemanticError> checkSemantics(Environment env) {
        //create the result
        ArrayList<SemanticError> res = new ArrayList<SemanticError>();

        //check semantics in the left and in the right exp

        res.addAll(left.checkSemantics(env));


        return res;
    }

    public Node typeCheck() {
        Node l = left.typeCheck();
        if (! ( FOOLlib.isSubtype(l,new BoolTypeNode())) ) {
            System.out.println("Incompatible types in equal");
            System.exit(0);
        }
        return new BoolTypeNode();
    }
    public String codeGeneration() {
        String l = FOOLlib.freshLabel();
        String End = FOOLlib.freshLabel();
        return  left.codeGeneration()+
                "push 1\n" +
                "beq " + l + "\n" +
                "push 1\n" +
                "b " + End + "\n" +
                l + ":\n" +
                "push 0\n" +
                End + ":\n";
    }

}