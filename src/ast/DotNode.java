package ast;

import lib.FOOLlib;
import parser.FOOLParser;
import util.Environment;
import util.SemanticError;
import util.VirtualTable;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class DotNode extends CallNode {

  public String className;
  public String methodName;
  public ArrayList <Node> args;
  private Node type;
  private ArrayList<Node> parlist = new ArrayList<Node>();
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node body;
  private int methodOffset;
  private String classType;


  public DotNode(String classN, String method, ArrayList<Node> arg,Node e) {
  	super(method,arg);
    className=classN;
    methodName =method;
    args=arg;
    body = e;
  }

  
  @Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {

	  ArrayList<SemanticError> res = new ArrayList<SemanticError>();
	  int j = env.scopeNumber;
	  STentry temp = null;
      LinkedHashMap<String, STentry> hm = env.symTable.get(env.scopeNumber);

     while (j>=0 && temp == null ) {
      	  hm = env.symTable.get(j--);
          temp = hm.get(className);
      }
	  ArrayList<Node> par = new ArrayList<Node>();
	  if(temp == null || !(temp.getType() instanceof ClassTypeNode)) {
	  	res.add(new SemanticError("object " + className +" not defined"));
	  }

	  else {			//se il metodo che sto chiamando è definito

		  ClassNode m = env.HMClass.get(((ClassTypeNode) hm.get(className).getType()).getType());
		  if(m == null){
		  	res.add(new SemanticError("the class type "+((ClassTypeNode) hm.get(className).getType()).getType() +" of class " + className + " is not defined!"));
		  	return res;
		  }

		  classType = m.id;			//mi salvo il tipo della classe
			if (VirtualTable.VTable.get(classType).getMethodByName(methodName)!= null) {  //se il metodo è presente nella Virtual Table lo cerco nella classe ed eventualmente nelle superclassi
				MethodNode methodCall = (MethodNode) env.HMClass.get(classType).getMethodByName(methodName);
				String suClass = env.HMClass.get(classType).getSuperClassId();
				while (methodCall == null && suClass != null) {
					methodCall = (MethodNode) env.HMClass.get(suClass).getMethodByName(methodName);
					suClass = env.HMClass.get(suClass).getSuperClassId();
				}
				if (methodCall != null && methodCall.getBody() instanceof DotNode) {   //controllo che i parametri della chiamata del metodo siano coerenti con quelli della del metodo
					for (int i = 0; i < args.size() ; i++) {
						if (!(FOOLlib.isSubtype(env.symTable.get(0).get(((IdNode)args.get(i)).getId()).getType(), methodCall.parlist.get(i).typeCheck()))) {
							System.out.println("The type of parameter " + ((IdNode) args.get(i)).getId() + " is not subtype of parameter " + ((ParNode) methodCall.parlist.get(i)).id);
							System.exit(0);
						}

						String s = ((ClassTypeNode) env.symTable.get(0).get(((IdNode) args.get(i)).getId()).getType()).getType();
						((ClassTypeNode) methodCall.parlist.get(i).typeCheck()).putType(s);
						((DotNode) methodCall.getBody()).classType = s;  //aggiusto il tipo dei parametri del metodo, utile quando il metodo prende in input classi e gli vengono passate sottoclassi delle classi definite
					}


				}
			}
		  boolean presente = false;
		  boolean arg = true;
			for (Node meth : m.getMethods()) {
				if (methodName.equals(((MethodNode) meth).id)) {
					presente = true;
					type = ((MethodNode) meth).type;
					if (((MethodNode) meth).parlist.size() != args.size()) arg = false;
					par = ((MethodNode) meth).parlist;
				 }
			  }

		  	STentry e= new STentry(hm.get(className).getNestinglevel(),new ArrowTypeNode(par,type),hm.get(className).getOffset());
			if (!presente){
				String superClassId = m.getSuperClassId();
				while(superClassId!= null){				//cerco il metodo nelle superclassi se non è presente nella classe

					ArrayList<Node> methods = env.HMClass.get(superClassId).getMethods();
					for (Node x : methods){
						if(((MethodNode)x).id.equals(id)){
							presente = true;
							type = ((MethodNode) x).type;
							e = new STentry(hm.get(className).getNestinglevel(),new ArrowTypeNode(((MethodNode) x).parlist,type),hm.get(className).getOffset());
						}
					}
					superClassId = env.HMClass.get(superClassId).getSuperClassId(); //mi salvo il tipo della superClasse
				}
			}

		  if (presente == false)			//se il metodo non è presente o il numero di parametri è sbagliato ritorno errore
			  res.add(new SemanticError("method " + methodName + " in object " + className + " not defined"));
		  else if (arg == false)
			  res.add(new SemanticError(methodName + " in object " + className + " not have " + args.size() + " parameters"));
		  else{
		  		hm = env.symTable.get(env.scopeNumber);
			  hm.put(methodName,e);
			  res.addAll(super.checkSemantics(env));


		  }
	  }

	  return res;
	}

  public String toPrint(String s) {
	String parlstr="";
	for (Node par:parlist)
	  parlstr+=par.toPrint(s+"  ");
	String declstr="";
	if (declist!=null) 
	  for (Node dec:declist)
	    declstr+=dec.toPrint(s+"  ");
    return s+"Fun:" + id +"\n"
		   +(type != null?type.toPrint(s+"  "): "")
		   +parlstr
	   	   +declstr
          +(body != null?body.toPrint(s+"  "): "" );
  }
  
  public Node typeCheck () {
	return super.typeCheck();
  }			//chiamo il typeCheck di callNode


	public String codeGeneration() {
		StringBuilder parCode = new StringBuilder();
		for (int i = args.size() - 1; i >= 0; i--)
			parCode.append(args.get(i).codeGeneration());

		StringBuilder getAR = new StringBuilder();
		int nfield = 0;
		int i = 1;
		if(VirtualTable.VTable.size()>0) {											//cerco l'offset dei metodi nella VirtualTable
			for (String s : VirtualTable.VTable.get(classType).getMethodKey()) {
				if (s.equals(methodName)) {
					nfield = VirtualTable.VTable.get(classType).getField().size();
					methodOffset = i + (nfield *2);
					break;
				}
				i = i+2 ;															// + 2 perchè i campi e i metodi sono nella forma "push" "field0" e "push" "method0"
			}
		}


		for (int j = 0; j < nestinglevel - entry.getNestinglevel(); j++)
			getAR.append("lw\n");

		return "lfp\n"                                  // carico il frame pointer
				+ parCode                               // carico i parametri
				+ "push " + entry.getOffset() + "\n"         // carico l'offset dell'oggetto nello scope di definizione
				+ "lfp\n"                               // carico il frame pointer
				+ getAR                                 // faccio gli lw necessari fino a trovarmi sullo stack l'indirizzo in memoria dell'oggetto
				+ "add\n"                               // faccio $fp + offset per ottenere l'indirizzo in memoria dell'oggetto
				+ "lw\n"                                // carico il valore dell'oggetto sullo stack
				+ "ssp\n"                              // copio il valore sopra (l'indirizzo di memoria nel quale si trova l'indirizzo della classe relativa)
				+ "lw\n"
				+ "push " + methodOffset  + "\n"   // carico l'offset del metodo
				+ "add" + "\n"
				+ "lc\n"                                // trovo l'indirizzo del metodo
				+ "js\n";                              // salto all'istruzione dove e' definito il metodo e salvo $ra
  }
  
}  
