package ast;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import util.Environment;
import util.SemanticError;
import lib.FOOLlib;

public class VarNode implements Node {

  public String id;
  public Node type;
  private Node exp;
  
  public VarNode (String i, Node t, Node v) {
    id=i;
    type=t;
    exp=v;
  }
  
  	@Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {
  	//create result list
  	  ArrayList<SemanticError> res = new ArrayList<SemanticError>();

  	  LinkedHashMap<String,STentry> hm = env.symTable.get(env.scopeNumber);
      /*  if (exp instanceof ConstructorNode){                                                //serve a vedere i metodi della sottoclasse nel caso in cui una classe chiama il costruttore di una sottoClasse
   	      if (!((ConstructorNode)exp).getId().equals(((ClassTypeNode)type).getType())) {
   	          this.typeCheck();
  	          type = exp.typeCheck();
           }
      }*/

        STentry entry = new STentry(env.nestingLevel,type, env.offset--); //separo introducendo "entry"

     if (hm.put(id, entry) != null)
            res.add(new SemanticError("Var id " + id + " already declared"));

    res.addAll(exp.checkSemantics(env));

    return res;
	}
  
  public String toPrint(String s) {
	return s+"Var:" + id +"\n"
	  	   +type.toPrint(s+"  ")  
           +exp.toPrint(s+"  "); 
  }
  
  //valore di ritorno non utilizzato
  @Override
  public Node typeCheck () {

      if (!(FOOLlib.isSubtype( exp.typeCheck(), type))) {
              System.out.println("incompatible value for variable " + id);
              System.exit(0);
      }

    return null;
  }
  
  public String codeGeneration() {
		return "\n//"+id+"\n" + exp.codeGeneration();
  }  
    
}  