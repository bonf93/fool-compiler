package ast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import parser.*;
import parser.FOOLParser.*;


import util.SemanticError;

public class FoolVisitorImpl extends FOOLBaseVisitor<Node> {

	LinkedHashMap<String, String> SuperClass = new LinkedHashMap<String, String>();

	String classe;

	@Override
	public Node visitLetInExp(LetInExpContext ctx) {
		
		//resulting node of the right type
		ProgLetInNode res;
		
		//list of declarations in @res
		ArrayList<Node> declarations = new ArrayList<Node>();
		
		//visit all nodes corresponding to declarations inside the let context and store them in @declarations
		//notice that the ctx.let().dec() method returns a list, this is because of the use of * or + in the grammar
		//antlr detects this is a group and therefore returns a list
		for(DecContext dc : ctx.let().dec()){
			declarations.add( visit(dc) );
		}
		
		//visit exp context
		Node exp = visit( ctx.exp() );
		
		//build @res accordingly with the result of the visits to its content
		res = new ProgLetInNode(declarations,  exp);
		
		return res;
	}
	
	@Override
	public Node visitSingleExp(SingleExpContext ctx) {
		
		//simply return the result of the visit to the inner exp
		return visit(ctx.exp());
		
	}

	@Override
	public Node visitClassExp(ClassExpContext ctx) {

		ArrayList<ClassNode> classList = new ArrayList<ClassNode>();
		ArrayList<Node> decList = new ArrayList<Node>();


		for (ClassdecContext c: ctx.classdec()){
			classList.add((ClassNode)visit(c));
		}
		if(ctx.let()!=null){
			for (DecContext d : ctx.let().dec()){
				decList.add(visit(d));
			}
		}
		Node exp = visit(ctx.exp());

		ProgClassNode progClass = new ProgClassNode(classList,decList,exp);

		return progClass;

	}


	@Override
	public Node visitNewClassExp(NewClassExpContext ctx) {
		ConstructorNode res;

		ArrayList<Node> param = new ArrayList<Node>();
		STentry entry;

		for (ExpContext exp : ctx.exp())
			param.add(visit(exp));
		if (SuperClass.get(ctx.ID().getText())!= null) {
			String sc = SuperClass.get(ctx.ID().getText());
			ClassTypeNode SN =new ClassTypeNode(ctx.ID().getText());
			ClassTypeNode TN = SN;
			while (sc != null){
				ClassTypeNode CN = new ClassTypeNode(sc);
				TN.putSuperClass(CN);
				TN = TN.getSuperClass();
				sc = SuperClass.get(sc);
			}
			entry = new STentry(0, new ArrowTypeNode(param, SN), 0);
		}

		else entry = new STentry(0,new ArrowTypeNode(param, new ClassTypeNode(ctx.ID().getText())),0);
		res = new ConstructorNode(ctx.ID().getText(),entry, param,0);

		return res;
	}

	@Override
	public Node visitDotClassExp(DotClassExpContext ctx) {
		ArrayList<Node> args = new ArrayList<Node>();


		if (ctx.methodCall().exp().size()>0) for (ExpContext x:ctx.methodCall().exp()) args.add(visit(x));

		Node e = null;
		if (ctx.methodCall().exp().size()>0)
		e = visit(ctx.methodCall().exp(0));
		return new DotNode(ctx.methodCall().ID(0).getText(),ctx.methodCall().ID(1).getText(),args,e);

	}
	@Override
	public Node visitMethodCallDec(MethodCallDecContext ctx) {
		ArrayList<Node> args = new ArrayList<Node>();

		if (ctx.methodCall().exp().size()>0) for (ExpContext x:ctx.methodCall().exp()) args.add(visit(x));

		Node e = null;
		if (ctx.methodCall().exp().size()>0)
			e = visit(ctx.methodCall().exp(0));
		return new DotNode(ctx.methodCall().ID(0).getText(),ctx.methodCall().ID(1).getText(),args,e);
	}
	
	@Override
	public Node visitVarasm(VarasmContext ctx) {

		Node typeNode = visit(ctx.vardec().type());

		Node expNode = visit(ctx.exp());
		
		//build the varNode
		return new VarNode(ctx.vardec().ID().getText(), typeNode, expNode);
	}
	
	@Override
	public Node visitFun(FunContext ctx) {
		
		//initialize @res with the visits to the type and its ID
		FunNode res = new FunNode(ctx.ID().getText(), visit(ctx.typefun()));
		
		//add argument declarations
		//we are getting a shortcut here by constructing directly the ParNode
		//this could be done differently by visiting instead the VardecContext
		int i=0;
		for(VardecContext vc : ctx.vardec()) {
			res.addPar(new ParNode(vc.ID().getText(), visit(vc.type()),i+1));
			i++;
		}
		
		//add body
		//create a list for the nested declarations
		ArrayList<Node> innerDec = new ArrayList<Node>();
		
		//check whether there are actually nested decs
		if(ctx.let() != null){
			//if there are visit each dec and add it to the @innerDec list
			for(DecContext dc : ctx.let().dec())
				innerDec.add(visit(dc));
		}
		
		//get the exp body
		Node exp = visit(ctx.exp());
		
		//add the body and the inner declarations to the function
		res.addDecBody(innerDec, exp);
		
		return res;		
		
	}
	
	@Override
	public Node visitType(TypeContext ctx) {

		if(ctx.getText().equals("int"))
			return new IntTypeNode();
		else if(ctx.getText().equals("bool"))
			return new BoolTypeNode();

		else if (!ctx.getText().equals("void")) {
			if (SuperClass.get(ctx.ID().getText())!= null) {
				String sc = SuperClass.get(ctx.ID().getText());
				ClassTypeNode SN =new ClassTypeNode(ctx.ID().getText());
				ClassTypeNode TN = SN;
				while (sc != null){
					ClassTypeNode CN = new ClassTypeNode(sc);
					TN.putSuperClass(CN);
					TN = TN.getSuperClass();
					sc = SuperClass.get(sc);
				}
				return SN;
			}
			else
				return new ClassTypeNode(ctx.ID().getText());
		}

		else return null;
	}

	@Override
	public Node visitTypefun(TypefunContext ctx) {
		if(ctx.getText().equals("int"))
			return new IntTypeNode();
		else if(ctx.getText().equals("bool"))
			return new BoolTypeNode();
		else if(ctx.getText().equals("void"))
			return new VoidTypeNode();

		else{
			if (SuperClass.get(ctx.ID().getText())!= null) {
				String sc = SuperClass.get(ctx.ID().getText());
				ClassTypeNode SN =new ClassTypeNode(ctx.ID().getText());
				ClassTypeNode TN = SN;
				while (sc != null){
					ClassTypeNode CN = new ClassTypeNode(sc);
					TN.putSuperClass(CN);
					TN = TN.getSuperClass();
					sc = SuperClass.get(sc);
				}
				return SN;
			}
			else
				return new ClassTypeNode(ctx.ID().getText());
		}

	}

	@Override
	public Node visitExp(ExpContext ctx) {

		if(ctx.right == null){
			//it is a simple expression
			return visit( ctx.left );

		}else if(ctx.MINUS()==null){
			//it is a binary expression, you should visit left and right
			return new PlusNode(visit(ctx.left), visit(ctx.right));
		} else{
			return new MinusNode(visit(ctx.left), visit(ctx.right));
		}
		
	}
	
	@Override
	public Node visitTerm(TermContext ctx) {
		//check whether this is a simple or binary expression
		//notice here the necessity of having named elements in the grammar
		if(ctx.right == null){
			//it is a simple expression
			return visit( ctx.left );
		}else if (ctx.DIV()==null){
			//it is a binary expression, you should visit left and right
			return new MultNode(visit(ctx.left), visit(ctx.right));
		}else{
			//it is a binary expression, you should visit left and right
			return new DivNode(visit(ctx.left), visit(ctx.right));
		}
	}
	
	
	@Override
	public Node visitFactor(FactorContext ctx) {
		//check whether this is a simple or binary expression
		//notice here the necessity of having named elements in the grammar

		if(ctx.right == null){
			//it is a simple expression
			if (ctx.NOT() != null){
				return new NotNode(visit(ctx.left));
			}

			return visit( ctx.left );
		}


		if (ctx.AND()!=null){
			//it is a binary expression, you should visit left and right
			return new AndNode(visit(ctx.left), visit(ctx.right));

		}
		else if (ctx.OR()!=null){
			//it is a binary expression, you should visit left and right
			return new OrNode(visit(ctx.left), visit(ctx.right));

		}
		else if (ctx.MAJOR()!=null){
			//it is a binary expression, you should visit left and right
			return new MajorNode(visit(ctx.left), visit(ctx.right));

		}
		else if (ctx.MAJOR()!=null){
			//it is a binary expression, you should visit left and right
			return new MajorNode(visit(ctx.left), visit(ctx.right));

		}

		else if (ctx.MINOR()!=null){
			//it is a binary expression, you should visit left and right
			return new MinorNode(visit(ctx.left), visit(ctx.right));

		}
		else if (ctx.MAJOREQ()!=null){
			//it is a binary expression, you should visit left and right
			return new MajorEqNode(visit(ctx.left), visit(ctx.right));

		}
		else if (ctx.MINOREQ()!=null){
			//it is a binary expression, you should visit left and right
			return new MinorEqNode(visit(ctx.left), visit(ctx.right));

		}
		else {
			return new EqualNode(visit(ctx.left), visit(ctx.right));
		}

	}


	@Override
	public Node visitIntVal(IntValContext ctx) {
		// notice that this method is not actually a rule but a named production #intVal
		
		//there is no need to perform a check here, the lexer ensures this text is an int
		return new IntNode(Integer.parseInt(ctx.INTEGER().getText()));
	}
	
	@Override
	public Node visitBoolVal(BoolValContext ctx) {
		
		//there is no need to perform a check here, the lexer ensures this text is a boolean

		return new BoolNode(Boolean.parseBoolean(ctx.getText()));
	}
	
	@Override
	public Node visitBaseExp(BaseExpContext ctx) {
		
		//this is actually nothing in the sense that for the ast the parenthesis are not relevant
		//the thing is that the structure of the ast will ensure the operational order by giving
		//a larger depth (closer to the leafs) to those expressions with higher importance
		
		//this is actually the default implementation for this method in the FOOLBaseVisitor class
		//therefore it can be safely removed here
		
		return visit (ctx.exp());

	}

	@Override
	public Node visitIfExp(IfExpContext ctx) {
		
		//create the resulting node
		IfNode res;
		
		//visit the conditional, then the then branch, and then the else branch
		//notice once again the need of named terminals in the rule, this is because
		//we need to point to the right expression among the 3 possible ones in the rule
		
		Node condExp = visit (ctx.cond);
		
		Node thenExp = visit (ctx.thenBranch);
		
		Node elseExp = visit (ctx.elseBranch);
		
		//build the @res properly and return it
		res = new IfNode(condExp, thenExp, elseExp);
		
		return res;
	}
	
	@Override
	public Node visitVarExp(VarExpContext ctx) {
		
		//this corresponds to a variable access

		return new IdNode(ctx.ID().getText(), classe);

	}
	
	@Override
	public Node visitFunExp(FunExpContext ctx) {
		//this corresponds to a function invocation
		
		//declare the result
		Node res;
		
		//get the invocation arguments
		ArrayList<Node> args = new ArrayList<Node>();
		
		for(ExpContext exp : ctx.exp())
			args.add(visit(exp));

		if(ctx.ID().getText().equals("print"))
			res = new PrintNode(args.get(0));
		
		else
			res = new CallNode(ctx.ID().getText(), args);
		
		return res;
	}

	@Override
	public Node visitClassdec(ClassdecContext ctx) {
		ClassNode res;

		classe = ctx.ID(0).getText();

		if(ctx.ID().size()>1){

			SuperClass.put(ctx.ID(0).getText(), ctx.ID(1).getText());

			String sc = SuperClass.get(ctx.ID(0).getText());
			ClassTypeNode SN =new ClassTypeNode(ctx.ID(0).getText());
			ClassTypeNode TN = SN;
			while (sc != null){
				ClassTypeNode CN = new ClassTypeNode(sc);
				TN.putSuperClass(CN);
				TN = TN.getSuperClass();
				sc = SuperClass.get(sc);
			}
			res = new ClassNode(ctx.ID(0).getText(), SN);
			res.setSuper( ctx.ID(1).getText());

		}
		else res =  new ClassNode(ctx.ID(0).getText(), new ClassTypeNode(ctx.ID(0).getText()));
		int i=0;
		for(VardecContext x: ctx.vardec()){
			res.addParam(new ParNode(x.ID().getText(),visit(x.type()), i+1));
			i++;
		}
		for(VarasmContext x: ctx.varasm()){
			res.addFields(new FieldNode(x.vardec().ID().getText(),visit(x.vardec().type()),visit(x.exp())));
		}
		for(FunContext x: ctx.fun()){
			res.addMethods((FunNode) visitFun(x));
		}

		classe = null;
		return res;
	}
}
