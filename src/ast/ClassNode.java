package ast;

import lib.FOOLlib;
import parser.FOOLParser.*;
import util.Environment;
import util.SemanticError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import util.VirtualTable;
import util.VirtualTableEntry;

public class ClassNode implements Node {

  public String id;
  private String superClassId = null;
  private Node type;
  private ArrayList<Node> paramsList = new ArrayList<Node>();
  private ArrayList<Node> fieldsList = new ArrayList<Node>();
  private ArrayList<Node> methodsList = new ArrayList<Node>();
  private ArrayList<Node> paramsListSuperClass = new ArrayList<Node>();


  public ClassNode(String className, ClassTypeNode t) {
		  id=className;
		  type = t;

	}

	public void setSuper(String superClass){
  		superClassId=superClass;
	}

	public void addParam(ParNode x){
		paramsList.add(x);
	}
	public void addFields(FieldNode x){
		fieldsList.add(x);
	}
	public void addMethods(FunNode x){
  		MethodNode m = new MethodNode(x.id,x.type, id, x.getParlist(), x.getDeclist(),x.getBody() );
		methodsList.add(m);
	}

	public ArrayList<Node> getFields(){
		return fieldsList;
	}
	public ArrayList<Node> getMethods(){ return methodsList;}

	public ArrayList<String> getFieldsName(){
		ArrayList<String> res = new ArrayList<String>();
		for(Node n: fieldsList){
			res.add(((FieldNode)n).id);
		}
		return res;
	}


  public Node getMethodByName(String id){
  	for (Node a: methodsList){
  		if (((MethodNode)a).id.equals(id)) return a;
	}
	return null;
  }


  public void changeMethod(String meth,Node x) {
	  for (int i = 0; i < methodsList.size(); i++) {
		  if (((MethodNode) methodsList.get(i)).id.equals(meth)) methodsList.set(i, x);
	  }
  }


	public String getSuperClassId(){
  		return superClassId;
	}


    @Override
    public ArrayList<SemanticError> checkSemantics(Environment env) {

		LinkedHashMap<String, STentry> hm = env.symTable.get(env.scopeNumber); // aggiungo alla symbol Table la entry relativa alla classe corrente

		env.scopeNumber++;

		ArrayList<SemanticError> res = new ArrayList<SemanticError>();
        LinkedHashMap<String, STentry> hmn = new LinkedHashMap<String, STentry>();

        env.nestingLevel++;
		env.offset = -2;

		if (env.HMClassType.put(id, null)  != null) {
			res.add(new SemanticError("Class name '" + id + "' has already been used."));
			System.out.println("Classi definite uguali");
			env.symTable.remove(env.nestingLevel--);
			return res;
		}

		String classl = FOOLlib.freshClassLabel();			//inizializzo la Virtual Table relativa alla classe corrente
		VirtualTableEntry temp = new VirtualTableEntry(classl, superClassId);
		VirtualTable.VTable.put(id,temp);
		STentry entry = new STentry(env.nestingLevel,new ArrowTypeNode(paramsList, type), env.offset--);
		hmn.put(id, entry);
		env.HMClass.put(id, this);
		env.HMClassType.put(id, (ClassTypeNode) type);

		/** SuperClass **/

		ArrayList<String> FieldSuperClass = new ArrayList<String>();
		ArrayList<String> MethodSuperClass = new ArrayList<String>();
		String SuperClass = superClassId;
        while (SuperClass != null) {   					// se la classe corrente è una sottoclasse di qualche classe controllo che nn ci siano errori semantici e aggiungo alla Virtual Table i campi e i metodi relativi
			if(env.HMClassType.get(SuperClass) == null) {
				res.add(new SemanticError("super class " + SuperClass + " is not defined"));
				env.scopeNumber--;
				env.nestingLevel--;
				return res;
			}

			if(paramsList.size() != ((ArrowTypeNode)hm.get(SuperClass).getType()).getParList().size()){
				res.add(new SemanticError("super class " + SuperClass + " not have "+ paramsList.size()+" parameters"));
				env.scopeNumber--;
				env.nestingLevel--;
				return res;
			}

			for (Node n:((ArrowTypeNode)hm.get(SuperClass).getType()).getParList()) paramsListSuperClass.add(n);

			boolean fieldPresente = false;

			for (int  i = env.HMClass.get(SuperClass).getFields().size()-1 ; i>=0 ;i--){  // controllo che i campi sovrascritti siano di tipo coerente con la superclasse e aggiungo i campi non sovrascritti alla Virtual Table
				FieldNode f =(FieldNode)env.HMClass.get(SuperClass).getFields().get(i);
				String s = f.id;
				STentry e =new STentry(0, f.type,0);
				for (Node n:fieldsList){
					if (s.equals(((FieldNode)n).id)) {
						fieldPresente = true;
						if (!(FOOLlib.isSubtype(((FieldNode) n).type, f.type))) {
							System.out.println("the field " + ((FieldNode) n).id + " is not subtype of field " + f.id + " of the super Class");
							System.exit(0);
						}
					}
				}
				if(! fieldPresente){
					hmn.put(s,e);
					if (!FieldSuperClass.contains(s))
						FieldSuperClass.add(s);
				}
				fieldPresente = false;
			}


			boolean methodPresente = false;															// controllo che i metodi sovrascritti siano di tipo coerente con la superclasse e abbiano lo stesso numero di parametri e aggiungo i metodi non sovrascritti alla Virtual Table
			for (int i = env.HMClass.get(SuperClass).getMethods().size()-1; i>=0 ;i--){
				MethodNode meth =((MethodNode)env.HMClass.get(SuperClass).getMethods().get(i));
				String s = meth.id;
				STentry e =new STentry(0, env.HMClass.get(SuperClass).getMethods().get(i),0);
				for (Node n:methodsList){
					if (s.equals(((MethodNode)n).id)){
						methodPresente = true;
						if (!(FOOLlib.isSubtype(((MethodNode) n).type, meth.type))){
							System.out.println("the method " + ((MethodNode) n).id + " is not subtype of method " + meth.id + " of the super Class " + SuperClass);
							System.exit(0);
						}

						if(((MethodNode) n).parlist.size() != meth.parlist.size()){
							System.out.println("the method " + ((MethodNode) n).id + " not have the same number of parameter of method " + meth.id + " of the super Class " +SuperClass);
							System.exit(0);
						}

						else {
							for (int k=0 ; k< ((MethodNode) n).parlist.size(); k++){
								if (!(FOOLlib.isSubtype(((MethodNode) n).parlist.get(k).typeCheck(),meth.parlist.get(k).typeCheck()))){
									System.out.println("the parameter "+ ((ParNode)((MethodNode) n).parlist.get(k)).id+ " of the method "+ ((MethodNode)n).id + " not is subtype of the parameter "+ ((ParNode)meth.parlist.get(k)).id +" of method "+ meth.id +" of the super class " +SuperClass);
									System.exit(0);
								}
							}
						}
					}
				}
				if(! methodPresente){
					hmn.put(s,e);
					if (!MethodSuperClass.contains(s))
						MethodSuperClass.add(s);
				}
				methodPresente = false;
			}

			SuperClass = env.HMClass.get(SuperClass).superClassId;
		}
			//aggiungo i campi e i metodi non sovrascritti alla Virtual Table nel solito ordine nel quale si presentano nella superClasse
		for (int i = FieldSuperClass.size()-1; i>=0; i--)VirtualTable.VTable.get(id).addField(FieldSuperClass.get(i),VirtualTable.VTable.get(superClassId).getFieldByName(FieldSuperClass.get(i)));
		for (int i = MethodSuperClass.size()-1; i>=0; i--)VirtualTable.VTable.get(id).addMethod(MethodSuperClass.get(i),VirtualTable.VTable.get(superClassId).getMethodByName(MethodSuperClass.get(i)));



		hm.put(id,entry);
		env.symTable.add(hmn);


        /*** Parameters ***/

		LinkedHashMap<String, STentry> hmpar = new LinkedHashMap<String, STentry>();			//per ogni parametro della classe, controllo che nn sia già definito e chiamo la checkSemantic

		if(paramsList.size()>0) {
			env.nestingLevel++;
			env.offset = -2;
			for (Node par : paramsList) {

				if (hmpar.put(((ParNode) par).id, new STentry(-1, -1)) != null) {                //controllo che nn ci siano parametri definiti più volte
					res.add(new SemanticError(
							"Field name '" + ((ParNode) par).id + "' for class '" + id + "' has already been used."));
					System.out.println("Parametri definiti uguali");
					env.symTable.remove(env.nestingLevel--);
					return res;
				}

				res.addAll(par.checkSemantics(env));

			}
		}

		/*** Fields ***/

		LinkedHashMap<String, STentry> hmfield = new LinkedHashMap<String, STentry>();					//per ogni campo della classe, controllo che nn sia definito più volte, chiamo la checkSemantic e lo aggiungo alla Virtual Table
		env.nestingLevel++;

		if(fieldsList.size()>0) {
			env.offset = -2;
			for (Node field : fieldsList) {

				if (hmfield.put(((FieldNode) field).id, new STentry(-1, -1)) != null) {                //controllo che nn ci siano parametri definiti più volte
					res.add(new SemanticError(
							"Field name '" + ((FieldNode) field).id + "' for class '" + id + "' has already been used."));
					System.out.println("Variabili definite uguali");
					env.symTable.remove(env.nestingLevel--);
					return res;
				}

				String s = FOOLlib.fieldLabel();
				VirtualTable.VTable.get(id).addField(((FieldNode)field).id, s);

				res.addAll(field.checkSemantics(env));

			}
		}


		/*** Methods ***/					//per ogni metodo della classe, controllo che nn sia definito più volte, chiamo il checkSemantic, lo aggiungo alla Virtual Table e aggiungo la STentry relativa alla Symbol Table
		LinkedHashMap<String, STentry> hmMethod = new LinkedHashMap<String, STentry>();
		LinkedHashMap<String, STentry> hmMeth = new LinkedHashMap<String, STentry>();


		env.ClassTable.put(id,hmMeth);
		if(methodsList.size()>0) {
			env.offset = -2;

			for (Node method : methodsList) {
				if (((MethodNode) method).id.equals(id)|| env.ClassTable.get(id).get(((MethodNode)method).id)!=null) {                //controllo che nn ci siano parametri definiti più volte
					res.add(new SemanticError(
							"Method name '" + ((MethodNode) method).id + "' for class '" + id + "' is incompatible."));
					return res;
				}
				else {

					String s = FOOLlib.freshMethodLabel();
					VirtualTable.VTable.get(id).addMethod(((MethodNode)method).id, s);
					env.scopeNumber++;
					env.symTable.add(hmMethod);
					res.addAll(method.checkSemantics(env));
				}
			}


		}

		if(paramsList.size()>0) env.nestingLevel--;
		env.nestingLevel--;

		env.nestingLevel--;
		env.symTable.remove(env.scopeNumber);			//rimuovo dalla Symbol Table la entry relativa alla classe corrente
		env.scopeNumber--;
        return res;
    }
  

  public String toPrint(String s) {
	String parStr="";
	String fieldStr = "";
	String methodStr = "";
	String res = "";

	for (Node par:paramsList)
	  parStr+=par.toPrint(s+"  ");
	for (Node field:fieldsList)
	   fieldStr+=field.toPrint(s+"  ");
	for (Node method : methodsList)
		methodStr+= method.toPrint(s+ "  ");

	  if (superClassId != null) res = s + " Class extends "+ superClassId;
	  else res = s + " Class";

	  return res + ":" + id +"\n"
			  +parStr
			  +fieldStr
			  +methodStr;
  }
  
  public Node typeCheck () {
  		if (superClassId!=null){				//se la classe corrente è una sottoclasse controllo che i parametri siano coerenti con il tipo di quelli della superclasse
			for (int i = 0; i< paramsList.size(); i++) {
				if( !FOOLlib.isSubtype(paramsList.get(i).typeCheck() ,paramsListSuperClass.get(i).typeCheck() )){
					System.out.println("the param " +paramsList.get(i).toPrint("") +" is wrong type");
					System.exit(0);
				}
			}
		}
	  for (Node par: paramsList)			//chiamo la typeCheck per ogni parametro, campo e metodo
		par.typeCheck();
	  for (Node field: fieldsList)
	  	field.typeCheck();
	  for (Node method: methodsList)
	  	method.typeCheck();

	return type;

  }
  
  public String codeGeneration() {

	    String parCode = "";
	    String fieldCode = "";
	    String methodCode = "";
	  	String classl=FOOLlib.freshClassLabel();


	  for (Node par : paramsList)					//ParNode
	    	parCode += par.codeGeneration();


        if (superClassId != null){			//se la classe corrente è una sottoclasse aggiungo i campi della superclasse non sovrascritti al codice della classe
	    	boolean fieldPresente = false;
	    	String t = new String();
	    	for (String s : VirtualTable.VTable.get(superClassId).getFieldKey()){
	    		for(Node field: fieldsList)
	    			if(((FieldNode)field).id.equals(s)) fieldPresente = true;
	    		if (!fieldPresente){
	    			t += "push "+ VirtualTable.VTable.get(superClassId).getFieldByName(s) + "\n";
				}
	    		fieldPresente = false;

			}
			fieldCode = t;

		}

	  for (Node field: fieldsList) {                //VarNode
		  String b= ((FieldNode)field).codeGeneration();
		  fieldCode += b;
		  String []a = b.split(" ");
	  }



	  if (superClassId != null){						//se la classe corrente è una sottoclasse aggiungo i metodi della superclasse non sovrascritti al codice della classe
		  boolean methodPresente = false;
		  String t = new String();
		  for (String s : VirtualTable.VTable.get(superClassId).getMethodKey()){
			  for(Node method: methodsList)
				  if(((MethodNode)method).id.equals(s)) methodPresente = true;
			  if (!methodPresente){
			  	t+= "push "+ VirtualTable.VTable.get(superClassId).getMethodByName(s) + "\n";
			  }
			  methodPresente= false;
		  }
		  methodCode = t;

	  }

	  for ( Node method: methodsList) {        //MethodNode
		  String b= method.codeGeneration();
		  methodCode+=b;
		  String []a = b.split(" ");
	  }

	  FOOLlib.putClassCode(classl+					//inserisco il codice relativa alla classe corrente in FOOLlib
			  ":\n"+
			  parCode+
			  fieldCode+
			  methodCode
			  );

	    return 	"push " + classl+ "\n";
  }
  
}  