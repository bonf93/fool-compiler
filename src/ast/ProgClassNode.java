package ast;

import lib.FOOLlib;
import org.stringtemplate.v4.ST;
import util.Environment;
import util.SemanticError;
import util.VirtualTableEntry;
import util.VirtualTable;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ProgClassNode implements Node {

    private ArrayList <ClassNode> classList;
    private ArrayList<Node> decList;
    private Node exp;

  public ProgClassNode(ArrayList<ClassNode> cl,ArrayList<Node> d, Node e) {
      classList = cl;
    decList=d;
    exp=e;
  }
  
  public String toPrint(String s) {
      String classStr="";
	String declstr="";
	for (ClassNode c : classList)
	    classStr += c.toPrint(s+"  ");
    for (Node dec:decList)
      declstr+=dec.toPrint(s+"  ");
	return s+"ProgClass\n" + classStr +"\n"+ declstr + "\n"+ exp.toPrint(s+ "  ") ;
  }
  
  @Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {

     env.scopeNumber++;

      LinkedHashMap<String,STentry> hm = new LinkedHashMap<String,STentry> ();

      hm.put("progClass", null);                        //aggiungo la entry "progClass" alla symbol Table
     env.symTable.add(hm);

      ArrayList<SemanticError> res = new ArrayList<SemanticError>();


      for (ClassNode cl: classList) {               // per ogni classe in classList chiamo la checkSemantic di quella classe
          if(res.size() ==0)
          res.addAll(cl.checkSemantics(env));
      }


      if(decList.size() > 0 && res.size()==0){          //per ogni dichiarazione nella declist chiamo la checkSemantic di quella dichiarazione // let
          env.nestingLevel=0;
          env.offset=-2;

    	  for(Node n : decList)
    		  res.addAll(n.checkSemantics(env));
      }


      if (res.size()==0){                           // se non ho riscontrati errori semantici fino a questo punto chiamo la checkSemantic sull' espressione // in
          env.nestingLevel=0;
          res.addAll(exp.checkSemantics(env));

      }

      env.symTable.remove(env.scopeNumber);         //rimuovo la Stentry della "progClass" dalla symbol Table
      env.scopeNumber--;


    /* for (ClassNode cn : classList){               // per ogni classe aggiorno i metodi che potrebbero aver subito modifiche nel checkSemantic //$
          for (Node a : env.HMClass.get(cn.id).getMethods()) cn.changeMethod(((MethodNode)a).id,a);
      }*/

      FOOLlib.reset();                              // riazzero tutte le variabili di FOOLlib
      return res;
	}

  
  public Node typeCheck () {

    for (ClassNode x:classList)                     //chiamo il typeCheck per ogni classe, dichiarazione e espressine
        x.typeCheck();
    for (Node dec: decList)
        dec.typeCheck();
    return exp.typeCheck();
  }
  
  public String codeGeneration() {
	  String classCode="";
	  String decCode = "";
	  for (Node classx:classList) {
	      String a= classx.codeGeneration();
          classCode += a;
          String []b = a.split(" ");

      }
	  for (Node dec : decList)
	      decCode += dec.codeGeneration();

	  return  "push 0"+                         //aggiungo all'inizio del codice "push 0", e poi inserisco il codice relativo alle dichiarazioni delle classi, dei relativi metodi e del resto del programma che è presente in FOOLlib
              decCode+
              "\n"+
			  exp.codeGeneration()+
              "halt\n"+
              FOOLlib.getCode()+
              FOOLlib.getClassCode()
              ;
  } 
  
  
    
}  