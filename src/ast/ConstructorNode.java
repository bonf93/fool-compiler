package ast;

import java.util.ArrayList;

import ast.Node;
import util.Environment;
import util.SemanticError;
import util.*;

public class ConstructorNode extends CallNode {

    public ConstructorNode(String i, STentry e, ArrayList<Node> p, int nl) {
        super(i, e, p, nl);
    }

    public String getId() {
        return id;
    }


    @Override
    public String toPrint(String s) {
        String parstr = "";

        for (Node par : parlist) {
            parstr += par.toPrint(s + "  ");
        }

        return s + "New:" + id + "\n" + parstr;
    }

    @Override
    public ArrayList<SemanticError> checkSemantics(Environment env) {


  ArrayList<SemanticError> res = new ArrayList<SemanticError>();

		if (env.HMClassType.get(id) == null) {
			res.add(new SemanticError("Class " + id + " has not been defined; cannot be instantiated."));
			return res;
		}

		res.addAll(super.checkSemantics(env)); //chiamo il checkSemantic di CallNode
		return res;
	}

	@Override
    public Node typeCheck() {
        return super.typeCheck();
    } //chiamo il typeCheck di CallNode

    @Override
    public String codeGeneration() {
        String temp= "";                    //aggiungo sullo stack l'indirizzo della classe istanziata e i suoi parametri
        for (Node arg: parlist)
            temp+=arg.codeGeneration();

        return temp +"push "+parlist.size()+"\n"+"push "+VirtualTable.VTable.get(id).getClassID()+"\nnew\n";
    }

}
