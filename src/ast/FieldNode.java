package ast;

import lib.FOOLlib;
import util.Environment;
import util.SemanticError;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class FieldNode implements Node {     //nodo per i campi statici delle classi

  public String id;
  public Node type;
  private Node exp;

  public FieldNode(String i, Node t, Node v) {
    id=i;
    type=t;
    exp=v;
  }
  
  	@Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {
  	//create result list
  	  ArrayList<SemanticError> res = new ArrayList<SemanticError>();

  	  LinkedHashMap<String,STentry> hm = env.symTable.get(env.scopeNumber);
      STentry entry = new STentry(env.nestingLevel,type, env.offset--); //aggiungo la STentry del campo alla symbol Table della classe relativa (scopeNumber)

     if (hm.put(id, entry) != null)                                     //se è gia presente nella symbol Table della classe ritorno errore perchè sto ridichiarando un campo già presente
            res.add(new SemanticError("Var id " + id + " already declared"));

    res.addAll(exp.checkSemantics(env));

    return res;
	}
  
  public String toPrint(String s) {
	return s+"Var:" + id +"\n"
	  	   +type.toPrint(s+"  ")  
           +exp.toPrint(s+"  "); 
  }
  
  @Override
  public Node typeCheck () {
    if (! (FOOLlib.isSubtype(exp.typeCheck(),type)) || (!(exp.typeCheck().toPrint("").equals(type.toPrint("")))) ){         //se l'espressione non è sottotipo del campo ritorno errore
      System.out.println("incompatible value for variable "+id);
      System.exit(0);
    }     
    return null;
  }
  
  public String codeGeneration() {
		String field = FOOLlib.fieldLabel();
		FOOLlib.putCode(field+":\n" + exp.codeGeneration()+"lra\n"+"js\n");             //salvo il valore di ritorno e salto all'idirizzo del campo
      return "push "+field+"\n";
  }  
    
}  