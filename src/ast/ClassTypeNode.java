package ast;

import util.Environment;
import util.SemanticError;

import java.util.ArrayList;

public class ClassTypeNode implements Node {            //classe per i tipi delle classi
    String type;
    ClassTypeNode superClass;
  public ClassTypeNode(String t) {
      type = t;

  }

  
  public String toPrint(String s) {
	return s+"ClassType " +type+ "\n";
  }

  public String getType(){
      return type;
  }

    public ClassTypeNode getSuperClass(){
        return superClass;
    }
    public void putSuperClass(ClassTypeNode SP){
      superClass = SP;
    }             //aggiugne una superClasse

    public void putType(String s){
      type = s;
    }

  //non utilizzato
  public Node typeCheck() {
    return null;
  }

  //non utilizzato
  public String codeGeneration() {
		return "";
  }
  
  @Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {

	  return new ArrayList<SemanticError>();
	}
  
}  