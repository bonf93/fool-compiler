package ast;
import java.util.ArrayList;

import util.Environment;
import util.SemanticError;
import lib.FOOLlib;
import util.VirtualTable;

public class CallNode implements Node {

  protected String id;
  protected STentry entry;
  protected ArrayList<Node> parlist;
  protected int nestinglevel;
  private int objectOffset;
  private int methodOffset;
  private boolean method = false;

  
  public CallNode (String i, STentry e, ArrayList<Node> p, int nl) {
    id=i;
    entry=e;
    parlist = p;
    nestinglevel=nl;
  }
  
  public CallNode(String text, ArrayList<Node> args) {
	id=text;
    parlist = args;
}

public String toPrint(String s) {
    String parlstr="";
	for (Node par:parlist)
	  parlstr+=par.toPrint(s+"  ");		
	return s+"Call:" + id + " at nestlev " + nestinglevel +"\n" 
           +entry.toPrint(s+"  ")
           +parlstr;        
  }

@Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {
		//create the result
		ArrayList<SemanticError> res = new ArrayList<SemanticError>();
		 int j=env.nestingLevel;            //controllo che la funzione  che sto cercando di chiamare esista cercando prima nella symbol Table e poi, se nn è stato trovato (sto chiamando un metodo di una superclasse)  nella struttura HMClassType presente in env
		 STentry tmp=null; 
		 while (j>=0 && tmp==null)
		     tmp=(env.symTable.get(j--)).get(id);
		 if (tmp==null){
             j = env.nestingLevel;
             ClassTypeNode Cnode= null;
             while (j >= 0 && Cnode == null) {
                 Cnode = env.HMClassType.get(id);
                 j--;
             }

             if (Cnode== null)
                 res.add(new SemanticError("Id " + id + " not declared"));
         }

		 else{
		     if (env.scopeNumber >0){
		         if (env.symTable.get(env.scopeNumber-1).keySet().contains(id)){
		             for (String s : env.symTable.get(env.scopeNumber-1).keySet()){
		                 if (VirtualTable.VTable.get(s)!=null){
		                     method = true;
                             int i =0;
                             for (String cl : VirtualTable.VTable.keySet()){
                                 if (cl.equals(s)){
                                     objectOffset = i;
                                     break;
                                 }
                             }
		                     int k =1;
		                     for (String meth: VirtualTable.VTable.get(s).getMethodKey()){
		                         if (meth.equals(id)){
		                             methodOffset= k;
		                             break;
                                 }
                                 k = k+2;
                             }
                         }
                     }
                 }
             }
             if (tmp.getType() instanceof MethodNode) tmp.addType(new ArrowTypeNode(parlist,((MethodNode) tmp.getType()).type));
			 this.entry = tmp;
			 for(Node arg : parlist)
				 res.addAll(arg.checkSemantics(env));
		 }

        this.nestinglevel = env.nestingLevel;

        for (Node par : parlist)
        res.addAll(par.checkSemantics(env));
		 return res;
	}

	@Override
  public Node typeCheck () {            //controllo che stia cercando di chiamare una funzione e che i parametri siano coerenti col la superclasse
	 ArrowTypeNode t=null;

     if (entry.getType() instanceof ArrowTypeNode ) t=(ArrowTypeNode) entry.getType();
     else {
       System.out.println("Invocation of a non-function "+id);
       System.exit(0);
     }
     ArrayList<Node> p = t.getParList();
     if ( !(p.size() == parlist.size()) ) {
       System.out.println("Wrong number of parameters in the invocation of "+id);
       System.exit(0);
     }
     for (int i=0; i<parlist.size(); i++) {
         if (!(FOOLlib.isSubtype((parlist.get(i)).typeCheck(), (p.get(i)).typeCheck()))){
             System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
             System.exit(0);
         }
     }
     return t.getRet();
  }
  
  public String codeGeneration() {
	    String parCode="";
	    for (int i=parlist.size()-1; i>=0; i--)
	    	parCode+=parlist.get(i).codeGeneration();
	    
	    String getAR="";
		for (int i=0; i<nestinglevel-entry.getNestinglevel(); i++)
		    	 getAR+="lw\n";


        if (!method )
		return "lfp\n"+ //CL
               parCode+
               "lfp\n"+getAR+ //setto AL risalendo la catena statica
               // ora recupero l'indirizzo a cui saltare e lo metto sullo stack
               "push "+entry.getOffset()+"\n"+ //metto offset sullo stack
		       "lfp\n"+getAR+ //risalgo la catena statica
			   "add\n"+
               "lw\n"+ //carico sullo stack il valore all'indirizzo ottenuto
		       "js\n";

      else return "lfp\n"                                  // carico il frame pointer
              + parCode                               // carico i parametri
              + "push " + objectOffset + "\n"
              + "lfp\n"                               // carico il frame pointer
              + "add\n"                               // faccio $fp + offset per ottenere l'indirizzo in memoria dell'oggetto
              + "lw\n"                                // carico il valore dell'oggetto sullo stack
              + "ssp\n"                              // copio il valore sopra (l'indirizzo di memoria nel quale si trova l'indirizzo della dispatch table)
              + "lw\n"                                // carico l'indirizzo della dispatch table sullo stack
              + "push " + methodOffset  + "\n"   // carico l'offset del metodo rispetto all'inizio della dispatch table
              + "add" + "\n"                          // carico sullo stack dispatch_table_start + offset
              + "lc\n"                                // trovo l'indirizzo del metodo
              + "js\n";                               // salto all'istruzione dove e' definito il metodo e salvo $ra
  }

    
}  