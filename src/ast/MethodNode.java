package ast;

import jdk.nashorn.internal.codegen.CompilerConstants;
import lib.FOOLlib;
import util.Environment;
import util.SemanticError;
import util.VirtualTable;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class MethodNode implements Node {

	public String id;
	public Node type;
	private String ownerClass;

	public ArrayList<Node> parlist = new ArrayList<Node>();
	private ArrayList<Node> declist = new ArrayList<Node>();
	private Node body;

	public MethodNode(String i, Node t, String o, ArrayList <Node> p, ArrayList<Node> d, Node b ) {
		id=i;
		type=t;
		ownerClass=o;
		parlist =p;
		declist = d;
		body = b;
	}

	public void addDecBody (ArrayList<Node> d, Node b) {
		declist=d;
		body=b;
	}
	public Node getBody(){
		return body;
	}

	public String getOwnerClass(){return ownerClass;}


	@Override
	public ArrayList<SemanticError> checkSemantics(Environment env) {

		//create result list
		ArrayList<SemanticError> res = new ArrayList<SemanticError>();


		LinkedHashMap<String,STentry> hm = env.symTable.get(env.scopeNumber);
		STentry entry = new STentry(env.nestingLevel,new ArrowTypeNode(parlist,type),env.offset--); //separo introducendo "entry"
		if ( hm.put(id,entry) != null )
			res.add(new SemanticError("Method id "+id+" already declared"));
		else{

			env.symTable.get(env.scopeNumber-1).put(id,entry);

			int oldOffset=env.offset;

			if (parlist.size()>0) {
				env.nestingLevel++;
				env.offset=-2;
				for (Node a : parlist) {
					ParNode arg = (ParNode) a;
					res.addAll(arg.checkSemantics(env));
				}
			}


			if(declist.size() > 0){
				env.nestingLevel++;
				env.offset = -2;
				for(Node n : declist) {
					VarNode var = (VarNode)n;
					res.addAll(var.checkSemantics(env));
				}
				env.nestingLevel--;
			}
			env.offset=oldOffset;

			if(parlist.size()>0)env.nestingLevel--;
			res.addAll(body.checkSemantics(env));
			env.symTable.remove(env.scopeNumber--);

		}

		return res;
	}


	public String toPrint(String s) {
		String parlstr="";
		for (Node par:parlist)
			parlstr+=par.toPrint(s+"  ");
		String declstr="";
		if (declist!=null)
			for (Node dec:declist)
				declstr+=dec.toPrint(s+"  ");
		return s+"Method:" + id +"\n"
				+type.toPrint(s+"  ")
				+parlstr
				+declstr
				+body.toPrint(s+"  ") ;
	}

	public Node typeCheck () {
		if (declist!=null)
			for (Node dec:declist)
				dec.typeCheck();
		if((body instanceof PrintNode) && type instanceof VoidTypeNode){
			return new VoidNode();
		}

		if ( !(FOOLlib.isSubtype(body.typeCheck(),type)) ){
			System.out.println("Wrong return type for function "+id);
			System.exit(0);
		}
		return null;
	}

	public String codeGeneration() {

		String declCode="";
		if (declist!=null) for (Node dec:declist)
			declCode+=dec.codeGeneration();

		String popDecl="";
		if (declist!=null) for (Node dec:declist)
			popDecl+="pop\n";



		String popParl="";
		for (Node dec:parlist)
			popParl+="pop\n";


		String methodl=FOOLlib.freshMethodLabel();
		FOOLlib.putCode(methodl+":\n"+
				"cfp\n"+ //setta $fp a $sp
				"lra\n"+ //inserimento return address
				declCode+ //inserimento dichiarazioni locali
				body.codeGeneration()+
				"srv\n"+ //pop del return value
				popDecl+
				"sra\n"+ // pop del return address
				"pop\n"+ // pop di AL
				popParl+
				"sfp\n"+  // setto $fp a valore del CL
				"lrv\n"+ // risultato della funzione sullo stack
				"lra\n"+
				"js\n" // salta a $ra*/
		);

		return "push "+ methodl +"\n";
	}

}

