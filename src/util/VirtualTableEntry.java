package util;

import javax.print.attribute.HashAttributeSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class VirtualTableEntry {
    private String classID;
    private String SuperClassID;
    private LinkedHashMap<String,String> fieldID;
    private LinkedHashMap<String,String> methodID;

    public  VirtualTableEntry (String cID, String scID/* ,LinkedHashMap<String,String> fID, LinkedHashMap<String,String> mID*/ ){
        classID = cID;
        SuperClassID = scID;
        fieldID = new LinkedHashMap<String, String>();
        methodID = new LinkedHashMap<String, String>();
        }
    public String getClassID(){
        return classID;
    }

    public ArrayList<String>getField(){
        ArrayList<String> c = new ArrayList<String>();
        for(String s: fieldID.keySet()) c.add(fieldID.get(s));
        return c;
    }

    public ArrayList<String> getFieldKey(){
        ArrayList<String> c = new ArrayList<String>();
        for(String s: fieldID.keySet()) c.add(s);
        return c;
    }
    public ArrayList<String>getMethod(){
        ArrayList<String> c = new ArrayList<String>();
        for(String s: methodID.keySet()) c.add(methodID.get(s));
        return c;
    }

    public ArrayList<String> getMethodKey(){
        ArrayList<String> c = new ArrayList<String>();
        for(String s: methodID.keySet()) c.add(s);
        return c;
    }

    public void addField(String fieldName, String fieldCode ){
        fieldID.put(fieldName,fieldCode);
    }

    public void addMethod(String methodName, String methodCode ){
        methodID.put(methodName,methodCode);
    }

    public String getFieldByName(String s){
        String res = fieldID.get(s);
        return res;
    }
    public String getMethodByName(String s){
        String res = methodID.get(s);
        return res;
    }

    public String getSuperClassID(){
        return SuperClassID;
    }

}
