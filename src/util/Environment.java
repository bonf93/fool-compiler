package util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import ast.ClassNode;
import ast.ClassTypeNode;
import ast.STentry;

public class Environment {
	
	public ArrayList<LinkedHashMap<String,STentry>> symTable = new ArrayList<LinkedHashMap<String,STentry>>();
	public LinkedHashMap<String, ClassTypeNode> HMClassType = new LinkedHashMap<String, ClassTypeNode>();
	public LinkedHashMap<String, ClassNode> HMClass = new LinkedHashMap<String, ClassNode>();
	public LinkedHashMap<String, LinkedHashMap<String,STentry>> ClassTable = new LinkedHashMap<String, LinkedHashMap<String, STentry>>();



	public int nestingLevel = -1;
	public int offset = 0;
	public int scopeNumber = -1;


	public void dumpEnv(){			// funzione che stampa su un file la Symbol entry
		FileWriter fw = null;
		try {
			fw = new FileWriter("/home/fulvio/Universita/compilatori&interpreti/idea-IC-181.4892.42/workspace/Ercolani/src/dumpsym.txt");
			for(LinkedHashMap<String,STentry>hm: symTable){
				fw.write("################################\n");
				for(String k: hm.keySet()){
					fw.write("Entry:\t" + k + "\n");
					if (hm.get(k) != null) fw.write(hm.get(k).toPrint("") + "\n");

					//}
				}
			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}
	
}
