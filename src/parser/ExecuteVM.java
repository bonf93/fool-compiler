package parser;


import ast.STentry;
import util.VirtualTable;
import util.VirtualTableEntry;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class ExecuteVM {

    public static final int CODESIZE = 1000;
    public static final int MEMSIZE = 1000;
    public static final int HEAPSIZE = 100;
    public static int HEAPFREE = 100;

    private int[] code;
    private int[] memory = new int[MEMSIZE];

    private int ip = 0;
    private int sp = MEMSIZE;

    private int hp = 1;
    private int fp = MEMSIZE;
    private int ra;
    private int rv;

    public ExecuteVM(int[] code) {
        this.code = code;
    }


    public void dump(){
        FileWriter fw = null;
        try {
            fw = new FileWriter("/home/fulvio/Universita/compilatori&interpreti/idea-IC-181.4892.42/workspace/Ercolani/src/dump.txt");
            for (int i = MEMSIZE-1; i> 0; i--) {
                fw.write(i+":\t"+memory[i] + "\n");
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
    public String cpu() {
        String ris = "";
        while (ip < MEMSIZE) {
            dump();
            int bytecode = code[ip++]; // fetch
            int v1,v2;

            int address;
            switch ( bytecode ) {
                case SVMParser.PUSH:
                    push( code[ip++] );
                    break;
                case SVMParser.POP:
                    pop();
                    break;
                case SVMParser.ADD :
                    v1=pop();
                    v2=pop();
                    push(v2 + v1);
                    break;
                case SVMParser.MULT :
                    v1=pop();
                    v2=pop();
                    push(v2 * v1);
                    break;
                case SVMParser.DIV :
                    v1=pop();
                    v2=pop();
                    if (v1 == 0) {
                        System.out.println("ERROR: division by zero");
                        System.exit(0);
                    } else {
                        push(v2 / v1);
                    }
                    break;
                case SVMParser.SUB :
                    v1=pop();
                    v2=pop();
                    push(v2 - v1);
                    break;
                case SVMParser.STOREW : //
                    address = pop();
                    memory[address] = pop();
                    break;
                case SVMParser.LOADW : //
                    push(memory[pop()]);
                    break;
                case SVMParser.AND :
                    address = code[ip++];
                    v1=pop();
                    v2=pop();

                    System.out.println("v1"+ v1);
                    System.out.println("v2"+ v2);
                    if (v1==1 && v2==1) ip = address;
                    break;
                case SVMParser.OR :
                    address = code[ip++];
                    v1=pop();
                    v2=pop();
                    System.out.println("v1"+ v1);
                    System.out.println("v2"+ v2);
                    if (v1==1 || v2==1) ip = address;
                    break;
                case SVMParser.BRANCH :
                    address = code[ip];
                    ip = address;
                    break;
                case SVMParser.BRANCHEQ : //
                    address = code[ip++];
                    v1=pop();
                    v2=pop();
                    if (v2 == v1) ip = address;
                    break;
                case SVMParser.BRANCHG :
                    address = code[ip++];
                    v1=pop();
                    v2=pop();
                    if (v2 > v1) ip = address;
                    break;
                case SVMParser.BRANCHL :
                    address = code[ip++];
                    v1=pop();
                    v2=pop();
                    if (v2 < v1) ip = address;
                    break;
                case SVMParser.BRANCHGREATQ :
                    address = code[ip++];
                    v1=pop();
                    v2=pop();
                    if (v2 >= v1) ip = address;
                    break;
                case SVMParser.BRANCHLESSEQ :
                    address = code[ip++];
                    v1=pop();
                    v2=pop();
                    if (v2 <= v1) ip = address;
                    break;
                case SVMParser.JS : //
                    address = pop();
                    ra = ip;
                    ip = address;
                    break;
                case SVMParser.STORERA : //
                    ra=pop();
                    break;
                case SVMParser.LOADRA : //
                    push(ra);
                    break;
                case SVMParser.STORERV : //
                    rv=pop();
                    break;
                case SVMParser.LOADRV : //
                    push(rv);
                    break;
                case SVMParser.LOADFP : //
                    push(fp);
                    break;
                case SVMParser.STOREFP : //
                    fp=pop();
                    break;
                case SVMParser.COPYFP : //
                    fp=sp;
                    break;
                case SVMParser.STOREHP : //
                    hp=pop();
                    break;
                case SVMParser.LOADHP : //
                    push(hp);
                    break;

                case SVMParser.NEW :                    //chiamata costruttore di una classe: salvo sullo heap la classe e i suoi parametri (sono sullo stack) e aggiorno le informazioni relative allo Heap libero
                    int startHp = hp;
                    int VirtualTableAddress = pop();
                    int nargs= pop();
                    int []args = new int[nargs];
                    if (HEAPFREE >= nargs +1) {
                        HEAPFREE = HEAPFREE-(nargs+1);

                        pushHp(VirtualTableAddress);
                        for (int i = 0; i < nargs; i++) {
                            args[i]= pop();
                        }
                        for (int i = nargs-1; i >=0; i--) {
                            pushHp(args[i]);
                        }
                    }
                    push(startHp);

                    break;

                case SVMParser.STORESP:                 //carico il valore puntato da sp sullo stack
                    push(memory[sp]);
                    break;

                case SVMParser.LOADCODE:            //carico sullo stack l'indirizzo ai cui dovrò saltare
                    int c = pop();
                    push(code[c]);
                   // System.out.println("LoadCode");
                    break;


                case SVMParser.PRINT :
                    ris = Integer.toString(memory[sp]);
                    System.out.println((sp<MEMSIZE)?memory[sp]:"Empty stack!");
                    break;
                case SVMParser.HALT :
                    return ris;
            }
        }
        return ris;
    }

    private int pop() {
        int x = memory[sp++];
        memory[sp-1]=0;

        return x;

    }

    private void push(int v) {
        memory[--sp] = v;
    }

    private void pushHp(int v){
        memory[hp++] = v;
    }

}