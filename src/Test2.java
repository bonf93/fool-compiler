import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import parser.ExecuteVM;
import parser.FOOLLexer;
import parser.FOOLParser;
import parser.SVMLexer;
import parser.SVMParser;
import util.Environment;
import util.SemanticError;
import ast.FoolVisitorImpl;
import ast.Node;
import util.VirtualTable;
import util.VirtualTableEntry;

public class Test2 {
	public static String main(String path) throws Exception {

		String gen = Methods.Compile(path);
		String ris = Methods.Interpreter(gen);
		return ris;

	}
	public static String compile(String path) throws Exception{
		return Methods.Compile(path);
	}
	public static String interpreter(String path) throws Exception{
		return Methods.Interpreter(path);
	}
	static  class Methods{
		private static Node ast;
		private static String fileName;

		public static String Compile (String path) throws Exception {

			fileName = path;

			FileInputStream is = new FileInputStream(fileName);
			ANTLRInputStream input = new ANTLRInputStream(is);
			FOOLLexer lexer = new FOOLLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);


			//SIMPLISTIC BUT WRONG CHECK OF THE LEXER ERRORS
			if (lexer.lexicalErrors > 0) {
				System.out.println("The program was not in the right format. Exiting the compilation process now");
			} else {

				FOOLParser parser = new FOOLParser(tokens);

				FoolVisitorImpl visitor = new FoolVisitorImpl();

				ast = visitor.visit(parser.prog()); //generazione AST

				Environment env = new Environment();
				ArrayList<SemanticError> err = ast.checkSemantics(env);

				if (err.size() > 0) {
					System.out.println("You had: " + err.size() + " errors:");
					for (SemanticError e : err)
						System.out.println("\t" + e);
				} else {


					System.out.println("Visualizing AST...");

					Node type = ast.typeCheck(); //type-checking bottom-up
					System.out.println(type.toPrint("Type checking ok! Type of the program is: "));


					// CODE GENERATION
					System.out.println(fileName);
					String code = ast.codeGeneration();
					BufferedWriter out = new BufferedWriter(new FileWriter(fileName+".asm"));
					out.write(code);
					out.close();
					System.out.println("Code generated! Assembling and running generated code.");
				}
			}

			return fileName+".asm";
		}

		public static String Interpreter (String file) throws Exception{
			fileName = file;

			FileInputStream isASM = new FileInputStream(fileName);
			ANTLRInputStream inputASM = new ANTLRInputStream(isASM);
			SVMLexer lexerASM = new SVMLexer(inputASM);
			CommonTokenStream tokensASM = new CommonTokenStream(lexerASM);
			SVMParser parserASM = new SVMParser(tokensASM);

			parserASM.assembly();
			System.out.println("You had: " + lexerASM.lexicalErrors + " lexical errors and " + parserASM.getNumberOfSyntaxErrors() + " syntax errors.");
			if (lexerASM.lexicalErrors > 0 || parserASM.getNumberOfSyntaxErrors() > 0) System.exit(1);

				System.out.println("Starting Virtual Machine...");
				ExecuteVM vm = new ExecuteVM(parserASM.code);
				String ris = vm.cpu();
				VirtualTable.VTable.clear();
				return ris;
			}
		}


	}

