package lib;

import ast.*;

public class FOOLlib {
  
  private static int labCount=0; 
  
  private static int funLabCount=0; 

  private static int classLabCount=0;

  private static int methLabCount=0;

  private static int fieldLabCount=0;


  private static String funCode="";
  private static String classCode="";
  private static String methodCode="";
  private static String fieldCode = "";


  //valuta se il tipo "a" è sottotipo al tipo "b"
  public static boolean isSubtype (Node a, Node b) {
    if (a instanceof ClassTypeNode){
      if (((ClassTypeNode) a).getType().equals(((ClassTypeNode) b).getType()))return true;
      if (((ClassTypeNode)a).getSuperClass()!= null) {
       return isSubtype(((ClassTypeNode) a).getSuperClass(), b);
      }
      else return false;
    }
    return a.getClass().equals(b.getClass()) ||
    	   ( (a instanceof BoolTypeNode) && (b instanceof IntTypeNode) );
  } 
  
  public static String freshLabel() { 
	return "label"+(labCount++);
  } 

  public static String freshFunLabel() { 
	return "function"+(funLabCount++);
  }

  public static String freshClassLabel() {
    return "class"+(classLabCount++);
  }

  public static String freshMethodLabel() {
    return "method"+(methLabCount++);
  }

  public static String fieldLabel() {
    return "field"+(fieldLabCount++);
  }

  public static void putCode(String c) { 
    funCode+="\n"+c; //aggiunge una linea vuota di separazione prima di funzione
  }

  public static void putClassCode(String c) {
    classCode+="\n"+c; //aggiunge una linea vuota di separazione prima di funzione
  }

  public static String getCode() { 
    return funCode;
  }

  public static String getClassCode() {
    return classCode;
  }

  public static String getMethodCode() {
    return methodCode;
  }

  public static void setMethLabCount(int x){
    methLabCount = x;
  }

  public static void reset(){
    labCount=0;

    funLabCount=0;

    classLabCount=0;

    methLabCount=0;

    fieldLabCount=0;
    funCode="";
    classCode="";
    methodCode="";
    fieldCode = "";
  }

}